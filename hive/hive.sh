#!/bin/bash
beeline
!connect jdbc:hive2://

#se você tiver problemas de travamento da VM, volte o parâmetro para mr
set hive.execution.engine;
set hive.execution.engine=spark;
set hive.execution.engine;

show databases;
create database userimages;
use userimages;

create table userimages_master(username_id string, img_id int, date_taken date, path_link string, xml_path string) row format delimited fields terminated by ';' location '/user/hadoopusr/output_test_cophir/';