#!/bin/bash

username=$1
password=$2
cophir_link=$3

if [ "$cophir_link" -lt 55 ];
then
	wget --user $username --password $password virserv02.isti.cnr.it/cophir/sapir_id_$cophir_link\_xml_r.tgz;
	echo "moving"
	mv sapir_id_$cophir_link\_xml_r.tgz /home/cohen/cophir
	cd /home/cohen/cophir
	echo "dir changed"
	tar -xvzf /home/cohen/cophir/sapir_id_$cophir_link\_xml_r.tgz -C .
	rm /home/cohen/cophir/sapir_id_$cophir_link\_xml_r.tgz
elif [ "$cophir_link" -lt 107 ];
then
	wget --user $username --password $password virserv02.isti.cnr.it/cophir/sapir_id_$cophir_link\_xml_n.tgz;
fi

echo "generating list of files"
rm /home/cohen/cophir/cophir.txt
cd /home/cohen/cophir
find . -type f > cophir.txt