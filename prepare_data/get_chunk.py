from sys import argv

start_line = int(argv[1])
end_line = int(argv[2])

output = open("/home/cohen/cophir/cophir_chunk.txt","w")

with open("/home/cohen/cophir/cophir.txt","r") as file:
	i = 0
	for line in file:
		if i >= start_line and i < end_line:
			output.write(line)
		i = i + 1
		if i > end_line:
			break


output.close()

