This is a project to load data images into Oracle and perform similarity queries. The data comes from xmls files that are parsed into hdfs and txt files.

1) Download zip files from cophir using download\_cophir.sh script. 2) Extract the zip into several xml files and generate a list of files (cophir.txt). 3) Use the full list at once or just a small chunk (get\_chunk.py). 4) Spark to parse xml all files into hdfs. 5) Sqlloader to load it on Oracle.
