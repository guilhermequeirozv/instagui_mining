from pyspark.sql import SparkSession

spark = SparkSession.builder.appName('oracleapp').enableHiveSupport().config('spark.jars','/opt/OJDBC8-Full/ojdbc8.jar').getOrCreate()
df = spark.read.format("jdbc")
			.option("url","jdbc:oracle:thin:instagui_oper/instagoper@//172.17.0.1:1521/ORCLPDB1")
			.option("dbtable","hellowspark")
			.option("user","instagui_oper")
			.option("password","instagoper")
			.option("driver","oracle.jdbc.driver.OracleDriver")
			.load()