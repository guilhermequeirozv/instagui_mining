#!/bin/bash

#export PYSPARK_PYTHON=/usr/bin/python3.7

echo "removing output dir"
hadoop dfs -rm -r /user/hadoopusr/output_test_cophir

echo "removing existing file"
hadoop dfs -rm -r cophir_chunk.txt

echo "inserting chunk"
hadoop dfs -copyFromLocal /home/cohen/cophir/cophir_chunk.txt

cd /home/hadoopusr/spark-2.4.0-bin-without-hadoop/bin
./spark-submit /home/cohen/Documentos/Projects/instagui_mining/spark/instagui_spark.py
